/*define Public svg building measures such as margin, colorScale */
var margin = {top: 60, right: 20, bottom: 60, left: 80},
    padding =80,
    aspect = 1243 /600 ,
    outerWidth = $("#chart-bubble").width(),
    outerHeight =outerWidth / aspect;
    //outerWidth = $("#report-2").width(),
    //outerHeight =$(window).height()- margin.top -margin.bottom,
var width = outerWidth - padding,
    height =  outerHeight - padding;
//define scales for
var xScale = d3.scale.linear().range([0,width-20]).domain([0,70]),
    yScale = d3.scale.log().range([height, 1]).domain([0.1,10000000]);
var termDict = {
  "001": "Auricular(otic)",
  "002": "Buccal",
  "003": "Cutaneous",
  "004": "Dental",
  "005": "Endocervical",
  "006": "Endosinusial",
  "007": "Endotracheal",
  "008": "Epidural",
  "009": "Extra-amniotic",
  "010": "Hemodialysis",
  "011": "Intra corpus cavernosum",
  "012": "Intra-amniotic",
  "013": "Intra-arterial",
  "014": "Intra-articular",
  "015": "Intra-uterine",
  "016": "Intracardiac",
  "017": "Intracavernous",
  "018": "Intracerebral",
  "019": "Intracervical",
  "020": "Intracisternal",
  "021": "Intracorneal",
  "022": "Intracoronary",
  "023": "Intradermal",
  "024": "Intradiscal (intraspinal)",
  "025": "Intrahepatic",
  "026": "Intralesional",
  "027": "Intralymphatic",
  "028": "Intramedullar (bone marrow)",
  "029": "Intrameningeal",
  "030": "Intramuscular",
  "031": "Intraocular",
  "032": "Intrapericardial",
  "033": "Intraperitoneal",
  "034": "Intrapleural",
  "035": "Intrasynovial",
  "036": "Intratumor",
  "037": "Intrathecal",
  "038": "Intrathoracic",
  "039": "Intratracheal",
  "040": "Intravenous bolus",
  "041": "Intravenous drip",
  "042": "Intravenous(not otherwise specified)",
  "043": "Intravesical",
  "044": "Iontophoresis",
  "045": "Nasal",
  "046": "Occlusive dressing technique",
  "047": "Ophthalmic",
  "048": "Oral",
  "049": "Oropharingeal",
  "050": "Other",
  "051": "Parenteral",
  "052": "Periarticular",
  "053": "Perineural",
  "054": "Rectal",
  "055": "Respiratory(inhalation)",
  "056": "Retrobulbar",
  "057": "Sunconjunctival",
  "058": "Subcutaneous",
  "059": "Subdermal",
  "060": "Sublingual",
  "061": "Topical",
  "062": "Transdermal",
  "063": "Transmammary",
  "064": "Transplacental",
  "065": "Unknown",
  "066": "Urethral",
  "067": "Vaginal"
};

function responsivefy(svg) {
    // get container + svg aspect ratio
    var width = parseInt(svg.style("width")),
        height = parseInt(svg.style("height")),
        aspect = width / height;

    // and call resize so that svg resizes on inital page load
    svg.attr("viewBox", "0 0 " + width + " " + height)
        .attr("perserveAspectRatio", "xMinYMid")
        .call(resize);

    d3.select(window).on("resize", resize);
    // get width of container and resize svg to fit it
    function resize() {
        var targetWidth = parseInt($('.chart-container').width());
        svg.attr("width", targetWidth);
        svg.attr("height", Math.round(targetWidth / aspect));
    }
}

//function responsivefy(svg){
//    var aspect = 1243 /600;
//    var width = parseInt(svg.width()),
//        height = parseInt(svg.height());
//    svg.attr("viewBox", "0 0 " + width + " " + height)
//        .attr("perserveAspectRatio", "xMinYMid")
//        .call(resize);
//    d3.select(window).on("resize", resize);
//    function resize() {
//        var targetWidth = parseInt($('.chart-container').width());
//        svg.attr("width", targetWidth);
//        svg.attr("height", Math.round(targetWidth / aspect));
//    }
//};

//init  2 types of chart container
var chartContainer = d3.select("#chart-bubble").append('svg').attr('width',outerWidth)
    .attr('height',outerHeight).call(responsivefy);



var bubbleChart=chartContainer
    .append('g')
    .attr('transform', "translate(80,20)")
    ;
//position translate tools
function translate(x, y){
  var xPosition = d3.scale.linear().domain([0,width]).range([0,width]);
  var yPosition = d3.scale.linear().domain([0,height]).range([0,height]);
  var result = "translate("+ xPosition(x) +","+ yPosition(y) + ")";
  return result;
}

function drawBubbleChart(data) {
  //define private scales
  var rScale = d3.scale.log().domain([1, 10000000]).range([10, 0.05 *width]);
  //create axis component
  var xAxis = d3.svg.axis()
      .scale(xScale)
      .ticks(0)
      .orient("bottom");
  var xAxisGroup = bubbleChart.append("g")
      .attr("class", "xAxis")
      .attr("transform", translate(0, height))
      .call(xAxis);
  xAxisGroup.append('text')
      .text("Drug Administration Methods (Alphabetically)")
      .attr('class', 'axis_label')
      .attr({"dx": width/2,"dy":'3em'});
  var yAxis = d3.svg.axis()
        .scale(yScale)
        .ticks(10, ",d")
        .orient("left");
  var yAxisGroup = bubbleChart.append("g")
      .attr("class", "yAxis")
      .attr("transform", translate(0, 0))
      .call(yAxis);
    yAxisGroup.append('text')
        .text("Number of Adverse Event Reports")
        .attr("class", "axis_label")
        .attr("transform", "rotate(-90)")
        .attr({"dx": -height/2,"dy":-60})
        ;
  // data bubble components
  //define node container
  var node = bubbleChart.selectAll(".node")
      .data(data)
      .enter().append("g")
      .attr("transform", function (d) {return translate(xScale(d.term)+15, yScale(d.count))})
      .attr("class", "node");

  //add circle into node
  node.append('circle')
      .attr("r", function (d) {return rScale(d.count);})
      .attr("class","circle");

  //add tooltip
  var tooltip =d3.select("#tooltip");
  var dynamicColor;

  //handle user operations
  //on bubble Mouse over & out
  node.on('mouseover', function (d) {
    //set mouseover color effect
    dynamicColor = this.style.fill;
    d3.select(this).style('fill','#E87352');
    tooltip.style("left", xScale(d.term) + "px")
        .style("top", d3.event.pageY + "px")
        .style({"opacity": '0.8','visibility':'visible' });
    //,'background-color':'#E87352'
    tooltip.select("#title")
        .text(d.title);
    tooltip.select("#value")
        .text('Total: ' + d3.format(',d')(d.count));

  });
  node.on('mouseout', function () {
    tooltip.style({'visibility':'hidden'});
    d3.select(this).style({'fill': dynamicColor});
  });
  //on bubble Click
  node.on('click', function (d) {
    //Switch to piechart by hiding bubble chart
    $('#bubble-title').hide();
    $('#chart-bubble svg').hide();
    $('#drill-note').hide();


    $('#pie-title').show();
    $('#rollup').show()
    tooltip.style({'visibility':'hidden'});
    drilldown(d);
  });
  return true;
}
function getChartData(err, response){
  if(err) throw err;
  var data = [];
  for (var i = 0; i < response.results.length; i++) {
    data.push({
      "title": termDict[response.results[i].term],
      "term": response.results[i].term,
      "count": response.results[i].count
    });
  }
  return data;
}

d3.json("https://api.fda.gov/drug/event.json?api_key=cKnbnLWO48t1jYAGWbiBFVImxF8K99DcWoFZXbgZ&search=patient.drug.drugadministrationroute:[001+TO+067]&count=patient.drug.drugadministrationroute", function (err,response) {
  var data = getChartData(err, response);
  drawBubbleChart(data);
});

function drilldown(node){
  var color = d3.scale.category10();
  var actions = {"1":"Drug withdrawn", "2":"Dose reduced", "3" : "Dose increased", "4" : "Dose not changed", "5" : "Unknown", "6" : "Not applicable"};
  var filter = node.term;
  var radius = d3.min([height,width])/3;
  d3.select("#chart-pie").style('display','block');
  var pieChart = d3.select('#chart-pie').append('svg')
    .attr('class','pie')
    .attr('width',width)
    .attr('height',height)
    .call(responsivefy);
  //remove previous nodes
  var arc = d3.svg.arc()
      .outerRadius(radius)
      .innerRadius(0);
  var pie = d3.layout.pie()
      .sort(null)
      .value(function(d){var value = d3.scale.linear().range([0,1000]).domain([0,10000]);return value(d.count);});

  d3.json("https://api.fda.gov/drug/event.json?api_key=cKnbnLWO48t1jYAGWbiBFVImxF8K99DcWoFZXbgZ&search=patient.drug.drugadministrationroute:" +filter+"&count=patient.drug.actiondrug", function(error, response){
    if(error) {
        pieChart.append('g').attr("transform", translate(width/2,height/2-20)).append('text').text('There is no data under your research: '+ node.title).attr('class','drill-exception');
        throw error;
    }
    //pieChart container
    var canvas = pieChart.append("g").attr('class','canvas').attr("transform","translate(" + (width/2+radius) + "," + (height)/2 +")");
    var sum=0;
    var data = (response, actions, function(){
      var result = [];
      for(var i= 0; i < response.results.length; i++){
        result.push({"title":actions[response.results[i].term], "term":response.results[i].term, "count":response.results[i].count});
        sum += response.results[i].count;
      }
      return result;
    }());
    //update the subtitle
    //d3.select("#pie-subtitle").text('Drug Administration Method: '+ node.title);
    var subtitle = pieChart.append('g').attr("transform", translate(width/2,20)).append('text').text('Drug Administration Method: '+ node.title).attr('class','pie-subtitle');
    var slices = canvas.append("g")
        .attr('class', 'slices');

    var labels = canvas.append('g')
        .attr('transform', "translate(0,30)")
        .attr('class', 'labels');

    var slice = slices.selectAll('path.slice')
        .data(pie(data))
        .enter().insert("path")
        .attr("class", "arc")
        .style({"fill": function(d){return color(d.data.title);}, 'opacity': '0.8'});

    slice.transition().duration(1000)
        .attrTween("d", function(d) {
          this._current = this._current || d;
          var interpolate = d3.interpolate(this._current, d);
          this._current = interpolate(0);
          return function(t) {
            return arc(interpolate(t));
          };
        })
    var legend= labels.selectAll('.legend')
        .data(pie(data)).enter()
        .append('g')
        .attr('transform',function(d,i){return 'translate('+ (-radius*2.5)+ ','+ (-30 *i + 10)+')'})
        .attr('class','legend');

    legend.append('rect')
        .attr('width',18)
        .attr('height',18)
        .style({"fill": function(d){return color(d.data.title);}, 'opacity': '0.8'});
    var text = legend.append('text')
        .attr('dy','1em')
        .attr('dx', '2em')
        .style('text-achor', 'end');
    text.append('tspan')
        .text(function(d){return d.data.title});

    function midAngle(d){

      return d.startAngle + (d.endAngle - d.startAngle)/2;
    }
    var tooltip = d3.select("#tooltip");
    slice.on("mouseover", function (d) {
      tooltip.style("left", (d3.event.pageX-200) + "px")
          .style("top", (d3.event.pageY) + "px")
          .style({"opacity": '1','visibility':'visible' })
          .select("#value")
          .text('Ratio: ' +  d3.format('%')(d.data.count/sum));
      tooltip.select("#title")
          .text('Action: ' + d.data.title);

    });
    slice.on("mouseout", function(d){
//
      tooltip.style({'visibility':'hidden' })
    });
  });
  return true;
}

$('#rollup').on('click', function(){
      //$('#pie-subtitle').text("");
      $('#bubble-title').show();
      $('#chart-bubble svg').show();
      $('#drill-note').show();

      $(this).hide();
      $('#pie-title').hide();
      $('#chart-pie').hide();
      $('#chart-pie svg').remove();

});

