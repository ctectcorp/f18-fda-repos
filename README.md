# CTEC GSA Agile 18F

Our application prototype is deployed at the following location: http://52.26.225.195/FDAReport/

## Our response to the 18F 4QTFHS150004 is attached as a word document: [Cybermedia 18F Technical Approach.docx](Cybermedia 18F Technical Approach.docx "18F 4QTFHS150004")

Following are links to the openFDA source datasets used to build our prototype:

Drug Administration Method and Drug Action Drill Down (the second link goes to Oral medicine -- changing the number after "patient.drug.drugadministrationroute" changes the method):

https://api.fda.gov/drug/event.json?api_key=PFcvg4csPSMeSlQIsXzFcKcG1symm2XiWiFgTGLo&count=patient.drug.drugadministrationroute
 
https://api.fda.gov/drug/event.json?api_key=PFcvg4csPSMeSlQIsXzFcKcG1symm2XiWiFgTGLo&search=patient.drug.drugadministrationroute:048&count=patient.drug.actiondrug
 
Patient Age (this link goes to ages 0-9) and Year Drill Down (for 2004)
 
https://api.fda.gov/drug/event.json?api_key=PFcvg4csPSMeSlQIsXzFcKcG1symm2XiWiFgTGLo&search=patient.patientonsetage:[0+TO+9]
 
https://api.fda.gov/drug/event.json?api_key=PFcvg4csPSMeSlQIsXzFcKcG1symm2XiWiFgTGLo&search=patient.patientonsetage:[0+TO+9]+AND+receivedate:[20040101+TO+20041231]&count=receivedate

###### Our folder structures are shown below:

	Folder: FDAReport (Code base)

	Folder: Evidence Docs

		File: Attachment E (Evidence Sheet) 

		**Folder: 01 Design**

			Folder: 01 Wireframes 

			Folder: 02 Style Guide 

			Folder: 03 Design Assets 

		**Folder: 02 Agile Development 

			Folder: 01 Iterative Deployment Screenshots 

			Folder: 02 Application Screenshots 

		**Folder: 03 Deployment

			Folder: 01 Continuous Integration

		**Folder: 04 Continuous monitoring				


![CTEC Corp Logo](http://ctec-corp.com/sites/all/themes/ctec/img/logo.png)
## CTEC CORP
######http://ctec-corp.com