# Deployment

The final code has been packaged into a container using Docker and has been made available using Docker hub:

https://registry.hub.docker.com/u/adheetctec/ctec-faers/

The application can be started on a client system by downloading the running the adheetctec/ctec-faers container:

1. SSH to server
2. If Docker software is not installed, please follow the instructions in the Docker website to install:
	http://docs.docker.com/linux/started/
3. Run:
	sudo docker run -d -p 9001:80 --name my-app adheetctec/ctec-faers /usr/sbin/apache2ctl -D FOREGROUND
	In the above command, instead of 9001, port 80 or 8080 can to used to provide external access to the application.
	The run command will download the container and dependencies if needed. 
	The application will be running on http://localhost:9001/#/.

